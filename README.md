# How to use: Guardian Calibration Tools   
   
Add the following line to the [UnityRoot]/Packages/manifest.json    
``` json     
"be.eloiexperiments.guardiancalibrationtools":"https://gitlab.com/eloistree/2019_07_15_guardiancalibrationtools",    
```    
--------------------------------------    
   
Feel free to support my work: http://patreon.com/eloistree   
Contact me if you need assistance: http://eloistree.page.link/discord   
   
--------------------------------------    
``` json     
{                                                                                
  "name": "be.eloiexperiments.guardiancalibrationtools",                              
  "displayName": "Guardian Calibration Tools",                        
  "version": "0.0.1",                         
  "unity": "2018.1",                             
  "description": "Tools to calibrate the real world based on the guardian system of Oculus Quest",                         
  "keywords": ["Script","Tool","init"],                       
  "category": "Script",                   
  "dependencies":{"be.eloiexperiments.randomtool": "0.0.1"}     
  }                                                                                
```    