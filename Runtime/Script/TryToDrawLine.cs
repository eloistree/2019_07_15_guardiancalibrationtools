﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TryToDrawLine : MonoBehaviour
{
    public float m_breakingDistance=0.05f;
    public Transform[] m_points;

    public List<LineInfo> m_lines;
    public Transform m_currentDirection;
    public Transform m_currentPoint;

    [System.Serializable]
    public class LineInfo {
        public Transform m_firstPoint;
        public Transform m_secondPoint;
        public Transform m_endPoint;
        public List<Transform> m_points;
        public void SetPoint(Transform first, Transform second) {
            m_firstPoint = first;
            m_secondPoint = m_endPoint = second;
            m_points.Clear();
        }
        public void AddPoint(Transform point) {
            m_points.Add ( point);
            m_endPoint = point;
        }

        public void DrawLine(float time) {

        }

        internal void SetEndPoint(Transform point)
        {
            m_endPoint = point;
        }
    }
    public LineInfo m_currentLine = new LineInfo() ;
    public int index = 0;
    public float timeBetweenFrame = 0.1f;

    IEnumerator Start()
    {
       
        for (int i = 0; i < m_points.Length-1; i++)
        {
            if (i == 0)
            {
                m_currentLine.SetPoint (m_points[i], m_points[i + 1]);
            }

            Vector3 directionPosition;
            Quaternion directionRotation;
            GetDirectionInfo(m_currentLine.m_firstPoint, m_currentLine.m_secondPoint, out directionPosition, out directionRotation);
            m_currentDirection.position = directionPosition;
            m_currentDirection.rotation = directionRotation;
            DrawLine(directionPosition, directionRotation, Color.blue, timeBetweenFrame,1f);

            Vector3 currentPosition;
            Quaternion currentRotation;
            GetDirectionInfo(m_points[i], m_points[i + 1], out currentPosition, out currentRotation);
            DrawLine(currentPosition, currentRotation, Color.yellow, timeBetweenFrame,0.5f);
            m_currentPoint.position = directionPosition;
            m_currentPoint.rotation = directionRotation;

            {//Draw Debug Lines

                Vector3 lineStartPosition;
                Quaternion lineStartRotation;
                GetDirectionInfo(m_currentLine.m_firstPoint, m_currentLine.m_secondPoint, out lineStartPosition, out lineStartRotation);
                DrawLine(lineStartPosition, lineStartRotation, Color.green, timeBetweenFrame,0.6f);

                Vector3 lineEndPosition;
                Quaternion lineEndRotation;
                GetDirectionInfo(m_currentLine.m_firstPoint, m_currentLine.m_endPoint, out lineEndPosition, out lineEndRotation);
                DrawLine(lineEndPosition, lineEndRotation, Color.magenta, timeBetweenFrame, 0.4f);
            }

            //Check if currentPosition is in the direction of line
            //If yes 
            //Add point and continues
            //Else 
            //break the line
            //create a new line




            Vector3 relocated = m_currentDirection.InverseTransformPoint(m_points[i].position);

           
            if (Mathf.Abs(relocated.x) > m_breakingDistance/4f)
            {
                LineInfo li = new LineInfo();
                li.m_firstPoint = m_currentLine.m_firstPoint;
                li.m_secondPoint = m_currentLine.m_secondPoint;
                li.m_endPoint = m_currentLine.m_secondPoint;
                m_lines.Add(li);
                m_currentLine.SetPoint(li.m_endPoint, m_points[i]);
            }
            else
            {
                m_currentLine.SetEndPoint(m_points[i]);
                Debug.DrawLine(m_currentDirection.position + m_currentDirection.right * m_breakingDistance / 2f, m_currentDirection.position + m_currentDirection.right * -m_breakingDistance / 2f, Color.red, 100);
                Debug.DrawLine(m_currentDirection.position, m_currentLine.m_endPoint.position, Color.red * 0.5f, timeBetweenFrame);

            }
            ////

            yield return new WaitForSeconds(timeBetweenFrame);
            DrawLines(timeBetweenFrame);
        }
        DrawLines(80);
        yield break;
    }
    private static void GetDirectionInfo(Transform from, Transform to, out Vector3 worldPosition, out Quaternion worldRotation)
    {
        Vector3 direction = (to.position - from.position);
        Quaternion rotation = Quaternion.LookRotation(direction);
        worldPosition = from.position;
        worldRotation = rotation;

    }

    private static void DrawLine(Vector3 position, Quaternion rotation, Color color, float time, float distance)
    {
        Debug.DrawLine(position,position + rotation * (Vector3.forward *distance), color, time);

    }

    private void DrawLines(float time)
    {
        for (int i = 0; i < m_lines.Count; i++)
        {
            Debug.DrawLine(m_lines[i].m_firstPoint.position, m_lines[i].m_endPoint.position,GetRandomColor(), time);
        }
    }

    private Color GetRandomColor()
    {
        return new Color(UnityEngine.Random.value, UnityEngine.Random.value, UnityEngine.Random.value);
    }

    private void OnValidate()
    {
        if (Application.isPlaying) {
            StartCoroutine(Start());

        }
    }
}
