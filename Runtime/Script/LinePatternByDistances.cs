﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinePatternByDistances : MonoBehaviour
{

    public Transform m_root = null;
    public Transform m_direction = null;
    public Transform [] m_points = null;
    public LinePatternInfo m_linePattern;
  

    void Update()
    {



        for (int i = 1; i < m_linePattern.m_points.Length; i++)
        {
            Debug.DrawLine(m_linePattern.m_points[i - 1].position, m_linePattern.m_points[i].position, Color.green);
            for (int j = 0; j < m_linePattern.m_pointsAndDistances.m_points.Length; j++)
            {
                Color random = new Color(UnityEngine.Random.value, UnityEngine.Random.value, UnityEngine.Random.value);
                DistancesOfPoint dp = m_linePattern.m_pointsAndDistances.m_points[j];
                for (int t = 0; t < m_linePattern.m_points.Length; t++)
                {
                    Debug.DrawLine(dp.m_point.position, m_linePattern.m_points[t].position, random);

                }

            }
        }
    }

    [System.Serializable]
    public class LinePatternInfo
    {
        public Transform m_root = null;
        public Transform m_direction = null;
        public Transform [] m_points;
        public GroupOfPointsDistances m_pointsAndDistances;

        public LinePatternInfo(Transform root, Transform direction, Transform [] points)
        {
            this.m_root = root;
            this.m_direction = direction;
            this.m_points = points;
            this.m_pointsAndDistances =new GroupOfPointsDistances(points);
        }
    }

    [System.Serializable]
    public class GroupOfPointsDistances
    {
        public float m_minDistance;
        public float m_maxDistance;
        public List<DistanceOfTransform> m_sections = new List<DistanceOfTransform>();

        public DistancesOfPoint[] m_points = new DistancesOfPoint[0];
        public GroupOfPointsDistances(Transform[] points)
        {
            m_points = new DistancesOfPoint[points.Length];
            for (int i = 0; i < m_points.Length; i++)
            {
                m_points[i] = new DistancesOfPoint(points[i], points);
            }
            for (int i = 1; i < points.Length; i++)
            {
                m_sections.Add(new DistanceOfTransform(points[i-1], points[i]));
            }
            DistanceOfTransform minDist;
            DistanceOfTransform maxDist;
            GetMinMaxDistance(out m_minDistance, out m_maxDistance, out minDist, out maxDist);

        }


        public void GetMinMaxDistance(out float min, out float max, out DistanceOfTransform minInfo, out DistanceOfTransform maxInfo) {
             minInfo = null;
             maxInfo = null;
             min = float.MaxValue;
             max = 0;
            for (int i = 0; i < m_points.Length; i++)
            {
                DistanceOfTransform vminInfo = null;
                DistanceOfTransform vmaxInfo = null;
                float vmin = float.MaxValue;
                float vmax = 0;

                m_points[i].GetMinMaxDistance(out vmin, out vmax, out  vminInfo, out  vmaxInfo);
                if (vmin < min)
                {
                    min = vmin;
                    minInfo = vminInfo;
                }
                if (vmax > max)
                {
                    max = vmax;
                    maxInfo = vmaxInfo;
                }

            }
        }
        public DistancesOfPoint GetFirstOne() {
            return m_points[0];
        }

    }


    [System.Serializable]
    public class DistancesOfPoint
    {

        public Transform m_point = null;
        public List<DistanceOfTransform> m_distances = new List<DistanceOfTransform>();

        public DistancesOfPoint(Transform from, Transform [] to)
        {
            m_distances.Clear();
            m_point = from;
            for (int i = 0; i < to.Length; i++)
            {
                if(from!=to[i])
                    m_distances.Add(new DistanceOfTransform(from, to[i]));
            }
        }

        internal void GetMinMaxDistance(out float min, out float max, out DistanceOfTransform minInfo, out DistanceOfTransform maxInfo)
        {
            minInfo = null;
            maxInfo = null;
            min = float.MaxValue;
            max = 0;

            for (int i = 0; i < m_distances.Count; i++)
            {
                float dist = m_distances[i].m_distance;
                if (dist < min)
                {
                    minInfo = m_distances[i];
                    min = dist;
                }
                if (dist > max)
                {
                    maxInfo = m_distances[i];
                    max = dist;
                }

            }


            
        }
    }


    [System.Serializable]
    public class DistanceOfTransform
    {

        public Transform m_from = null;
        public Transform m_to = null;
        public float m_distance = 0;

        public DistanceOfTransform(Transform from, Transform to)
        {
            m_from = from;
            m_to = to;
            m_distance = Vector3.Distance(from.position, to.position);
        }
    }

#if UNITY_EDITOR
    private void OnValidate()
    {
        m_linePattern = new LinePatternInfo(m_root, m_direction, m_points);


    }
#endif
}
