﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardianRecalibrationBasedOnDistance : MonoBehaviour
{

    public Transform [] m_points;
    public LinePatternByDistances m_linePattern;
    [Header("Test")]
    public List<Possibilities> m_possibilities;


    public void Start()
    {
        float min = m_linePattern.m_linePattern.m_pointsAndDistances.m_minDistance;
        float max = m_linePattern.m_linePattern.m_pointsAndDistances.m_maxDistance;
        for (int i = 0; i < m_points.Length; i++)
        {

            m_possibilities.Add(CreatePossibilityFrom(m_points[i], m_points, min, max));
        }
    }

    private Possibilities CreatePossibilityFrom(Transform point, Transform[] points,  float min, float max)
    {
        Possibilities p = new Possibilities();
        p.m_point = point;
        for (int i = 0; i < points.Length; i++)
        {
            float dist = Vector3.Distance(point.position, points[i].position);
            if( dist>min && dist<max)
                p.m_rangePossibilities.Add(points[i]);
        }
        return p;
    }

    [System.Serializable]
    public class Possibilities
    {
        public Transform m_point;
        public List<Transform> m_rangePossibilities;
    }


}
