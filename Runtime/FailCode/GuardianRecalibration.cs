﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardianRecalibration : MonoBehaviour
{

    public Transform m_targetToAffect;
    public Transform m_targetRecalibrationPosition;
    public GroupOfPointDistance m_recalibrationInfo = new GroupOfPointDistance();
    public Transform[] m_qualibrationPoints = new Transform[0];
    public float m_appoximationMeter = 0.1f;
    [System.Serializable]
    public class GroupOfPointDistance
    {

        public Transform m_targetPoint = null;
        public Vector3[] m_pointsOffset = new Vector3[0];
    }

    // Start is called before the first frame update
    void Start()
    {
        Recalibration();
    }

    private void Recalibration()
    {
        // m_targetToAffect.position=
    }

    // Update is called once per frame
    void Update()
    {

        for (int i = 1; i < m_qualibrationPoints.Length; i++)
        {
            Debug.DrawLine(m_qualibrationPoints[i - 1].position, m_qualibrationPoints[i].position, Color.yellow);
        }
    }

#if UNITY_EDITOR
    private void OnValidate()
    {
        if (m_recalibrationInfo == null)
            m_recalibrationInfo = null;
        m_recalibrationInfo.m_targetPoint = m_targetRecalibrationPosition;
        m_recalibrationInfo.m_pointsOffset = new Vector3[m_qualibrationPoints.Length];
        for (int i = 0; i < m_qualibrationPoints.Length; i++)
        {
            m_recalibrationInfo
                .m_pointsOffset[i] =
                m_recalibrationInfo
                .m_targetPoint.
                InverseTransformPoint(
                    m_qualibrationPoints[i].position);
        }
    }
#endif
}
